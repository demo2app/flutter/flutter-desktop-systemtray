import 'package:flutter/material.dart';

import 'package:system_tray/system_tray.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _systemTray = SystemTray();

  @override
  void initState() {
    super.initState();
    initSystemTray();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  String getTrayImagePath(String imageName) {
    return './assets/$imageName.ico';
  }

  Future<void> initSystemTray() async {
    await _systemTray.initSystemTray(
      iconPath: getTrayImagePath('app_icon'),
    );

    _systemTray.setToolTip('Hello World');
  }
}
